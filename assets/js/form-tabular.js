function add_form_rows(id, def, add, title, prefix, count) {
    var form = document.getElementById(id);
    var i = 0;
    do {
        --count;
        i++;
        add_form_row(form, def, title, prefix, i);
    } while (count);
    document.getElementById(add).onclick = function() {
        i++;
        add_form_row(form, def, title, prefix, i);
    }
}
function add_form_row(el, def, title, pref, n) {
    var formrow = document.createElement('div');
    var t = document.createElement('strong');
    t.innerHTML = title + ' ' + n;
    formrow.appendChild(t);
    formrow.className = 'row';
    for (var i = 0; i < def.length; i++) {
        var group = document.createElement('div');
        group.className = 'field';
        var item = def[i];
        var id = item.name + '_' + n;
        var label = document.createElement('label');
        label.innerHTML = item.label || item.name;
        label.htmlFor = id;
        switch (item.type) {
        case 'select':
            var field = document.createElement('select');
            for (var j in item.values) {
                if (item.values.hasOwnProperty(j)) {
                    var o = document.createElement('option');
                    o.value = j;
                    o.innerHTML = item.values[j];
                    field.appendChild(o);
                }
            }
            break;
        case 'textarea':
            var field = document.createElement('textarea');
            if (item.placeholder) {
                field.placeholder = item.placeholder;
            }
            break;
        default:
            var field = document.createElement('input');
            field.type = item.type;
            if (item.placeholder) {
                field.placeholder = item.placeholder;
            }
            break;
        }
        field.id = id;
        field.name = pref + '_' + item.name + '_' + n;
        field.className = 'form-control';
        field.classList.add(item.style);
        field.classList.add('auto-generated');
        group.appendChild(label);
        group.appendChild(field);
        formrow.appendChild(group);
    }
    el.appendChild(formrow);
}

